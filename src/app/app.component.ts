import {Component, Input, Output, EventEmitter, ChangeDetectionStrategy, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'ngxPagination Deme';
  collection = [];
  constructor() {
    for (let i = 1; i <= 100; i++) {
      this.collection.push(`Angular ${i}.0`);
    }
  }

  // every thing below is for the second pagination
  @Input() id: string;
  @Input() maxSize = 7;
  @Input() previousLabel = 'Previous';
  @Input() nextLabel = 'Next';
  @Input() screenReaderPaginationLabel = 'Pagination';
  @Input() screenReaderPageLabel = 'page';
  @Input() screenReaderCurrentLabel = `You're on page`;
  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();
  private _directionLinks = true;
  private _autoHide = false;
  get directionLinks(): boolean {
      return this._directionLinks;
  }
  set directionLinks(value: boolean) {
      this._directionLinks = !!value && <any>value !== 'false';
  }
  get autoHide(): boolean {
      return this._autoHide;
  }
  set autoHide(value: boolean) {
      this._autoHide = !!value && <any>value !== 'false';
  }
}
